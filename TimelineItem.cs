﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveC
{
    public class TimelineItem
    {
        public DateTime date;
        public int newDailyCases;
        public int newDailyDeaths;
        public int totalCases;
        public int recoveries;
        public int totalDeaths;
    }
}
