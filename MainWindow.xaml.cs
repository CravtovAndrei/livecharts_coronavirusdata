﻿using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Defaults;
using LiveCharts.Helpers;
using LiveCharts.Wpf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LiveC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StringBuilder sb = new StringBuilder(); //this is your data

        //From CSV
        List<double> actualCasesCol = new List<double>();
        List<double> dayLocalTransferCol = new List<double>();
        List<double> runningTotalDeathsCol = new List<double>();

        //FROM JSON
        List<double> _newDailyCases = new List<double>();
        List<double> _newDailyDeaths = new List<double>();
        List<double> _totalCases = new List<double>();
        List<double> _totalDeaths = new List<double>();
        List<double> _totalRecoveries = new List<double>();

        List<DateTime> dates = new List<DateTime>();
        DateTime start = new DateTime(2020, 03, 07);
        DateTime end = new DateTime(2020, 05, 05);

        public SeriesCollection SeriesCollection { get; set; }
        public DateTime InitialDateTime { get; set; }
        public Func<double, string> Formatter { get; set; }


        public SeriesCollection Series { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            SizeChanged += delegate
            {
                cartesianChart.Width = Window.GetWindow(this).Width - 20;
                cartesianChart.Height= Window.GetWindow(this).Height - 90;
            };

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            FileInfo info = new FileInfo(@"D:\\C#Projects\\LiveC\\MoldovaCoronavirusSpread.xlsx");

            ParseCoronaVirusApi();

            using (ExcelPackage xlPackage = new ExcelPackage(info))
            {
                var myWorksheet = xlPackage.Workbook.Worksheets.First(); //select sheet here
                var totalRows = myWorksheet.Dimension.End.Row; //sometimes value here is wrong
                var totalColumns = myWorksheet.Dimension.End.Column;

                for (int rowNum = 1; rowNum <= totalRows; rowNum++) //select starting row here
                {
                    var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                    sb.AppendLine(string.Join(",", row));
                }
                actualCasesCol = ReturnColumn(myWorksheet, totalRows);
                dayLocalTransferCol = ReturnColumn(myWorksheet, totalRows, 2);
                runningTotalDeathsCol = ReturnColumn(myWorksheet, totalRows, 6);

                for (var dt = start; dt <= end; dt = dt.AddDays(1))
                {
                    dates.Add(dt);
                }

            }

            DrawDates();


        }

        private void ParseCoronaVirusApi()
        {
            WebClient client = new WebClient();
            using (client)
            {
                var json = client.DownloadString("https://api.thevirustracker.com/free-api?countryTimeline=MD");
                var result = JsonConvert.DeserializeObject(json);
                JObject j = JObject.Parse(json);
                var infor = j["countrytimelinedata"][0]["info"];
                var timelineItems = j["timelineitems"][0].Children();

                foreach (var t in timelineItems)
                {
                    try
                    {
                        var a = t.Children();
                        var itemsList = a.ToList();
                        var dailyCases = (double)itemsList[0]["new_daily_cases"];
                        var newDailyDeaths = (double)itemsList[0]["new_daily_deaths"];
                        var totalCases = (double)itemsList[0]["total_cases"];
                        var totalRecoveries = (double)itemsList[0]["total_recoveries"];
                        var totalDeaths = (double)itemsList[0]["total_deaths"];


                        _newDailyCases.Add(dailyCases);
                        _newDailyDeaths.Add(newDailyDeaths);
                        _totalCases.Add(totalCases);
                        _totalRecoveries.Add(totalRecoveries);
                        _totalDeaths.Add(totalDeaths);
                    }
                    catch
                    {

                    }
                }
            }
        }

        private List<double> ReturnColumn(ExcelWorksheet myWorksheet, int totalRows, int colNumber = 1)
        {
              var tempList = new List<double>();
            for (int rowNum = 2; rowNum <= totalRows; rowNum++)
            {

                //var col = myWorksheet.Cells[rowNum, colNumber].Select(c => c.Value.ToString());
                var col = myWorksheet.Cells[rowNum, colNumber];

                foreach (var a in col)
                {
                    try
                    {
                        if(a.Value != null)
                        {
                            double n;
                            double.TryParse(a.Value.ToString(), out n);
                            tempList.Add(n);
                        }else
                        {
                            tempList.Add(0);
                        }
                    }
                    catch(Exception e)
                    {
                        string s = e.Message;
                    }
                }
            }
            return tempList;
        }

        private void DrawDates()
        {
            //Days
            var dayConfig = Mappers.Xy<DateModel>()
              .X(dateModel => dateModel.DateTime.Ticks / TimeSpan.FromDays(1).Ticks)
              .Y(dateModel => dateModel.Value);
            //and the formatter


            List<DateModel> dateModelsActualCases = new List<DateModel>();
            List<DateModel> dateModelsActualDeaths = new List<DateModel>();
            List<DateModel> dateModelsLocalTransfer = new List<DateModel>();
            List<DateModel> dateModelTotalRecoveries = new List<DateModel>();
            List<DateModel> dateModelNewDailyDeaths = new List<DateModel>();
            List<DateModel> newDailyCases= new List<DateModel>();

            dateModelsActualCases = GenerateDateModelFromList(_totalCases);
            dateModelsActualDeaths = GenerateDateModelFromList(_totalDeaths);
            dateModelsLocalTransfer = GenerateDateModelFromList(dayLocalTransferCol);
            dateModelTotalRecoveries= GenerateDateModelFromList(_totalRecoveries);
            dateModelNewDailyDeaths = GenerateDateModelFromList(_newDailyDeaths);
            newDailyCases = GenerateDateModelFromList(_newDailyCases);

            Series = new SeriesCollection(dayConfig)
            {
                new LineSeries
                {
                    Title = "Actual cases",
                    Values = dateModelsActualCases.AsChartValues(),
                    Fill = Brushes.Transparent
                },

                new LineSeries
                {
                    Title = "Actual Deaths",
                    Values = dateModelsActualDeaths.AsChartValues(),
                    Fill = Brushes.Transparent
                },

                new LineSeries
                {
                    Title = "Local Transfer",
                    Values = dateModelsLocalTransfer.AsChartValues(),
                    Fill = Brushes.Transparent
                },

                new LineSeries
                {
                    Title = "Total Recoveries",
                    Values = dateModelTotalRecoveries.AsChartValues(),
                    Fill = Brushes.Transparent
                },

                new LineSeries
                {
                    Title = "DailyDeaths",
                    Values = dateModelNewDailyDeaths.AsChartValues(),
                    Fill = Brushes.Transparent
                },

                new LineSeries
                {
                    Title = "Daily Cases",
                    Values = newDailyCases.AsChartValues(),
                    Fill = Brushes.Transparent
                }

            };

            //  Formatter = value => new DateTime((long)(value * TimeSpan.FromDays(1).Ticks)).ToString("d");
            //Func<double, string> formatFunc = (x) => string.Format("{0:0.000}", x);
            Formatter = value => new DateTime((long)(value *
            TimeSpan.FromDays(1).Ticks)).ToString(@"yyyy-MM-dd");

            cartesianChart.Series = Series;
            //RankGraphAxisX.LabelFormatter = formatFunc;
            cartesianChart.AxisX.Add(new Axis
            {
                LabelFormatter = Formatter
            });


            DataContext = this;
        }

        private List<DateModel> GenerateDateModelFromList(List<double> list)
        {
            var tempList = new List<DateModel>();
            while(list.Count != dates.Count)
            {
                list.Add(list[list.Count - 1]);
            }


            for (int i = 0; i < dates.Count; i++)
            {
                var m = new DateModel
                {
                    DateTime = dates[i],
                    Value = list[i]
                };

                tempList.Add(m);
            }
            return tempList;
        }
    }
    public struct DateModel
    {
        public System.DateTime DateTime { get; set; }
        public double Value { get => _value; set
            {
                if(value == null)
                {
                    _value = 0;
                }
                else
                {
                    _value = value;
                }
            }}

        double _value;
    }
}
